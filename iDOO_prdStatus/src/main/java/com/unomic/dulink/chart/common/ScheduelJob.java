/*
 * 
 */
package com.unomic.dulink.chart.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.domain.InventoryVo;
import com.unomic.dulink.chart.service.InventoryService;
import static java.lang.Math.toIntExact;

// TODO: Auto-generated Javadoc
/**
 * The Class ScheduelJob.
 * 
 * 
 * 
 */
@Component
@Repository
public class ScheduelJob extends SqlSessionDaoSupport {
	
	/** The inventory service. */
	@Autowired
	InventoryService inventoryService;
	
	/** The inventory vo. */
	private InventoryVo inventoryVo;
	
	
	/** The Constant namespaceOrder. */
	private final static String namespaceOrder = "com.unomic.dulink.inventory.";
	
	/** The Constant namespaceChart. */
	private final static String namespaceChart = "com.unomic.dulink.chart.";
	
	
	private final static String namespaceKpi = "com.unomic.dulink.kpi.";
	
//	private final static String namespaceReaOrder = "com.unomic.dulink.order.";
	
	/*@Scheduled(cron="00 35 08 * * * ")
	public void storeDeviceSummaryN() {
		
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, -1);
		
		String sDate = dayTime.format(cal.getTime());
		
		SqlSession sql = getSqlSession();
		List<ChartVo> dataListDay = new ArrayList<ChartVo>();
		List<ChartVo> dataListLatest = new ArrayList<ChartVo>();
		List<ChartVo> dataList = new ArrayList<ChartVo>();
		
		try {
			dataListDay = sql.selectList(namespaceKpi + "selectOperationStatusDay", sDate);
			dataListLatest = sql.selectList(namespaceKpi + "selectOperationStatus", sDate);
			
			int sizeLatest = dataListLatest.size();
			int sizeDay = dataListDay.size();
			
			for(int i=0;i<sizeLatest;i++) {
				
				for(int j=0;j<sizeDay;j++) {
					ChartVo chartVo = new ChartVo();
					if(dataListLatest.get(i).getDvcId().equals(dataListDay.get(j).getDvcId())) {
						chartVo.setWorkIdx(1); 
						chartVo.setWorkDate(sDate);
						chartVo.setDvcId(dataListLatest.get(i).getDvcId());
						chartVo.setTgRunTime(String.valueOf(Integer.parseInt(dataListLatest.get(i).getTgRunTime())/2));
						chartVo.setInCycleTime(dataListLatest.get(i).getInCycleTime() - dataListDay.get(j).getInCycleTime());
						chartVo.setWaitTime(dataListLatest.get(i).getWaitTime() - dataListDay.get(j).getWaitTime());
						chartVo.setAlarmTime(dataListLatest.get(i).getAlarmTime() - dataListDay.get(j).getAlarmTime());
						chartVo.setNoConnectionTime(dataListLatest.get(i).getNoConnectionTime() - dataListDay.get(j).getNoConnectionTime());
						chartVo.setOpTime(String.valueOf(Integer.parseInt(dataListLatest.get(i).getOpTime()) - Integer.parseInt(dataListDay.get(j).getOpTime())));
						chartVo.setAvgCyleTime(dataListLatest.get(i).getAvgCyleTime() - dataListDay.get(j).getAvgCyleTime());
						chartVo.setCuttingTime(dataListLatest.get(i).getCuttingTime() - dataListDay.get(j).getCuttingTime());
						dataList.add(chartVo);
					}else {
						
					}
				}
			}
			
			sql.insert(namespaceKpi+"insertOperationStatus", dataList);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	
	@Scheduled(cron="00 20 20 * * * ")
	public void storeDeviceSummaryD() {
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		
		String sDate = dayTime.format(date.getTime());
		
		SqlSession sql = getSqlSession();
		List<ChartVo> dataList = new ArrayList<ChartVo>();
		
		try {
			dataList = sql.selectList(namespaceKpi + "selectOperationStatus", sDate);
			int size = dataList.size();
			for(int i=0;i<size;i++) {
				dataList.get(i).setWorkIdx(2);
				dataList.get(i).setTgRunTime(String.valueOf(Integer.parseInt(dataList.get(i).getTgRunTime())/2));
			}
			sql.insert(namespaceKpi+"insertOperationStatus", dataList);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}*/
	
	/**
	 * Finish stock schduel.
	 * @author John.Joe
	 * @serialData 2017-11-30
	 * @version 0.1
	 * @exception Exception.class
	 * 
	 */
	@Scheduled(cron="00 30 08 * * * ")
	public void finishStockSchduel() {
		inventoryVo = new InventoryVo();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String sDate = dayTime.format(date);
		
		
		Calendar cal = Calendar.getInstance();
		  
		cal.setTime(date);
		cal.add(Calendar.DATE, -1);
		String today=dayTime.format(cal.getTime());
		cal.getTime();
		System.out.println("date ::"+date);
		System.out.println("sDate ::"+sDate);
		System.out.println("getTime ::"+cal.getTime());
		System.out.println("뺀날짜 ::"+today);

//		today = "2018-04-15";
		inventoryVo.setDate(today);
		
		System.out.println("최종 날짜"+inventoryVo.getDate());
		
		
		SqlSession sql = getSqlSession();
		List<InventoryVo> dataList = new ArrayList<InventoryVo>();
		
		try {
			dataList = sql.selectList(namespaceOrder + "getStockStatusDay", inventoryVo);
			int size =dataList.size();
			for(int i=0;i<size;i++) {

				dataList.get(i).setOhdCnt(dataList.get(i).getIniohdCnt()+dataList.get(i).getRcvCnt()-dataList.get(i).getNotiCnt()-dataList.get(i).getIssCnt());
//				dataList.get(i).setOhdCntL(dataList.get(i).getIniohdCntL()+dataList.get(i).getRcvCntL()-dataList.get(i).getNotiCntL()-dataList.get(i).getIssCntL());
				
				int iss= 0;
				int noti= 0;

				//라우팅 R삭이 없을 경우 Line소재 총 재고 구하기
				if(dataList.get(i).getMinOpr().equals("0020")) {
					for(int j=0; j<size; j++) {
						if(dataList.get(i).getPrdNo().equals(dataList.get(j).getPrdNo())){
							iss+=dataList.get(j).getIssCntL();
							noti+=dataList.get(j).getNotiCntL();
							
						}
					}
				}else{
					for(int j=0; j<size; j++) {
						if(dataList.get(i).getPrdNo().equals(dataList.get(j).getPrdNo())){
							iss+=dataList.get(j).getIssCntL();
							noti+=dataList.get(j).getNotiCntL();
							
						}
					}
				}
				dataList.get(i).setOhdCntL(dataList.get(i).getIniohdCntL()+dataList.get(i).getRcvCntL()-noti-iss);
			
				dataList.get(i).setOhdCntR(dataList.get(i).getIniohdCntR()+dataList.get(i).getRcvCntR()-dataList.get(i).getNotiCntR()-dataList.get(i).getIssCntR());
				dataList.get(i).setOhdCntM(dataList.get(i).getIniohdCntM()+dataList.get(i).getRcvCntM()-dataList.get(i).getNotiCntM()-dataList.get(i).getIssCntM());
				dataList.get(i).setOhdCntC(dataList.get(i).getIniohdCntC()+dataList.get(i).getRcvCntC()-dataList.get(i).getNotiCntC()-dataList.get(i).getIssCntC());
				dataList.get(i).setOhdCntP(dataList.get(i).getIniohdCntP()+dataList.get(i).getRcvCntP()-dataList.get(i).getNotiCntP()-dataList.get(i).getIssCntP());

				System.out.println("품번 : "+dataList.get(i).getItem()+" , C 총재고 : " + dataList.get(i).getOhdCntC() + " ,,, 촐고 :" + iss);
				
				/*dataList.get(i).setOhdCnt(dataList.get(i).getIniohdCnt()+dataList.get(i).getRcvCnt()-dataList.get(i).getNotiCnt()-dataList.get(i).getIssCnt());
				dataList.get(i).setOhdCntL(dataList.get(i).getIniohdCntL()+dataList.get(i).getRcvCntL());
				dataList.get(i).setOhdCntR(dataList.get(i).getIniohdCntR()+dataList.get(i).getRcvCntR());
				dataList.get(i).setOhdCntM(dataList.get(i).getIniohdCntM()+dataList.get(i).getRcvCntM()-dataList.get(i).getNotiCntM()-dataList.get(i).getIssCntM());
				dataList.get(i).setOhdCntC(dataList.get(i).getIniohdCntC()+dataList.get(i).getRcvCntC()-dataList.get(i).getNotiCntC()-dataList.get(i).getIssCntC());
				dataList.get(i).setOhdCntP(dataList.get(i).getIniohdCntP()+dataList.get(i).getRcvCntP()-dataList.get(i).getNotiCntP()-dataList.get(i).getIssCntP());*/
				dataList.get(i).setDate(today);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			int size = (int) sql.selectOne(namespaceOrder + "checkStockData", dataList.get(0).getDate());//마감 확인
			if(size>0) {
				sql.delete(namespaceOrder+"deleteStockData", dataList.get(0).getDate());
			}
			sql.insert(namespaceOrder + "BertStckMasterSave", dataList);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}
	
	/**
	 * nonOperationCalculator
	 * @author John.Joe
	 * @serialData 2017-12-01
	 * @version 0.1
	 * @throws ParseException 
	 * @exception Exception.class
	 * 
	 */
	@Scheduled(cron="00 22 18 * * * ")
	public void nonOperationCalculator() throws ParseException {
		
		SqlSession sql = getSqlSession();
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat dayTime1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = dayTime.format(cal.getTime());
		System.out.println(date);
		ChartVo chartVo = new ChartVo();
		chartVo.setSDate(date);
		List<ChartVo> dataList = new ArrayList<ChartVo>();
		dataList = sql.selectList(namespaceChart + "getWorkerWatingTime", chartVo);
		int size = dataList.size();
		
		List<ChartVo> waitList = new ArrayList<ChartVo>();
		List<ChartVo> temp = new ArrayList<ChartVo>();
		ChartVo temp2 = new ChartVo();
		temp2=null;
		logger.info("비 가동 계산을 준비 합니다.");
		logger.info("dataList의 사이즈는 : " + size);
		for(int i=0; i<size;i++){
			if(temp2==null){
				temp2=dataList.get(i);
			}else{
				//여기에 제외할 시간 ( 휴식시간 등 ) 기입해서 조건 맞추기
				Date temp2eDate = dayTime1.parse(temp2.getEDate());
				long temp2Time = temp2eDate.getTime();
				
				Date dataListeDate = dayTime1.parse(dataList.get(i).getEDate());
				long dataTime = dataListeDate.getTime();
				long minute = (dataTime - temp2Time) / 60000;

				if(temp2.getDvcId().equals(dataList.get(i).getDvcId()) && temp2.getWorkIdx() == 
						dataList.get(i).getWorkIdx() && 
						temp2.getChartStatus().equals(dataList.get(i).getChartStatus()) && minute == 2){
					temp2.setEDate(dataList.get(i).getEDate());
				}else{
					temp2.setChartStatus(dataList.get(i-1).getChartStatus());
					temp.add(temp2);
					temp2=null;
				}
			}
		}
		
		
		for(int i=0; i<temp.size(); i++){
			Date sDate = null;
			Date eDate = null;
			try {
				sDate = dayTime1.parse(temp.get(i).getSDate());
				eDate = dayTime1.parse(temp.get(i).getEDate());
			} catch (ParseException e) {
				// TODO 자동 생성된 catch 블록
				e.printStackTrace();
			}
			
			long diff = eDate.getTime() - sDate.getTime();
			System.out.println("diff is : " + diff);
			if(diff > 300000){
				if(temp.get(i).getEmpCd()==null) {
					temp.get(i).setEmpCd("0");
				}
				temp.get(i).setWaitTime(toIntExact(diff)/1000/60);
				waitList.add(temp.get(i));
				System.out.println(temp.get(i).toString());
			}
		}
		logger.info("비 가동 계산을 마무리 합니다.");
		logger.info("=================================");
		for(int i=0;i<waitList.size();i++) {
			logger.info("작업자 : " + waitList.get(i).getName()+" 장비 : " + waitList.get(i).getOprNo() + " 비가동 시간 : " + 
		waitList.get(i).getTime() +"분  시작 : " + waitList.get(i).getSDate() + " 종료시간 : " + waitList.get(i).getEDate());
		}
		logger.info("=================================");
		try {
			sql.delete(namespaceChart + "deleteWaitTime", date);
			sql.insert(namespaceChart + "insertWaitTime", waitList);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}//end
	
//	@Scheduled(cron="0 0/1 * * * ? ")
//	@Scheduled(cron="*/10 * * * * *")
//	public void workCntTest(){
	/*
		Date today = new Date();
		List<ChartVo> dataList = new ArrayList<ChartVo>();
	        
	    SimpleDateFormat date = new SimpleDateFormat("yyyy/MM/dd");
	    SimpleDateFormat time = new SimpleDateFormat("HH:mm");
	    
	    SqlSession sql = getSqlSession();
	    
	    String tgDate=date.format(today); //오늘 날짜 ex) : 2018/01/16
	    tgDate = tgDate.replace("/", "-"); // 치환 2018-01-16
	    String Testtime="17:30";

	    System.out.println("10초마다 실행");
		System.out.println(today);
		System.out.println("변환날짜 ::"+tgDate);
		System.out.println("Date: "+date.format(today));
		System.out.println("Time: "+time.format(today));
		System.out.println("Testtime: "+Testtime);
		
		System.out.println("value com ::" + time.format(today).equals(Testtime));;
		dataList = sql.selectList(namespaceChart + "tbDvcTgCnt" ,tgDate);
		System.out.println("dataList ::" + dataList + "size ::" + dataList.size());
		
		for(int i=0; i<dataList.size(); i++) {
			System.out.println(dataList.get(i).getStartWorkTime());
			//작업시작시간이 같을때
			if(dataList.get(i).getStartWorkTime().equals(Testtime)) {
				ChartVo chartVo= new ChartVo();
				int cnt=0;
				chartVo.setDvcId(dataList.get(i).getDvcId());
				chartVo.setTgDate(dataList.get(i).getTgDate());
				chartVo.setPrdNo(dataList.get(i).getPrdNo());
				chartVo.setWorkIdx(dataList.get(i).getWorkIdx());
				chartVo.setStartWorkTime(dataList.get(i).getStartWorkTime());
				System.out.println(chartVo);
				
				//작업자 변경시 오늘 만들어져있는 갯수 가져오기
				cnt = (int) sql.selectOne(namespaceChart + "tgCntstartEqual", chartVo);
				System.out.println("int ::::"+cnt);
				//시작 작업 갯수 집어 넣기
				//				sql.update(namespaceChart + "tbDvcTgCntUpdate" ,chartVo);
			}
		}*/
		
//	}
}
