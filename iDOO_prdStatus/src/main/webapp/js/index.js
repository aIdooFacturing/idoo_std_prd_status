$(()=>{
    setEl()
    bindEvt()
})

const bindEvt = () =>{
    $(".unos_input").keyup(chkKeyCd)
}

const chkKeyCd = evt => {
    if(evt.keyCode == 13){ // check enter key event
        location.href = 'popMainMenu.do'
    }
}

const setEl = () =>{
    $(".unos_input").css({
        "background-color" : "rgba(0,0,0,0)",
        "border" : "0",
        "color" : "black",
        "inline" : "0",
        "outline" : "0",
        "border-bottom" : getElSize(10) + "px solid lightgray",
        "padding" : getElSize(15) + "px",
        "font-size": getElSize(200) + "px",
        "transition" : "1s"
    }).focus(()=>{
        $(".unos_input").css({
            "border-bottom" : getElSize(10) + "px solid red"
        })
    }).blur(()=>{
        $(".unos_input").css({
            "border-bottom" : getElSize(10) + "px solid lightgray"
        })
    })

    $(".unos_input").css({
        "margin-left" : (width / 2) - ($(".unos_input").width() / 2),
        "margin-top" : (height / 2) - ($(".unos_input").height() / 2) - $("#header").height(),
        "display" : "inline"
    }).focus()
}