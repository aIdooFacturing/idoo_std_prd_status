<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
<!-- <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script> -->
<%-- <script src="${ctxPath}/js/dataTables/js/jquery-1.11.2.min.js"></script> --%>
<script src="${ctxPath}/js/dataTables/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="${ctxPath}/js/dataTables/css/jquery.dataTables.css">
<script type="text/javascript">
	var csvOutput;
	$(function(){
		today();
		$("#tabs").tabs();
	});
	
	function today(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth() +1));
		var day = addZero(String(date.getDate()));
		
		$("#visitorForm input[id='sdate']").val(year + "-" + month);
		$("#visitorForm input[id='edate']").val(year + "-" + month);
		$(".sdate").val(year + "-" + month + "-" + day);
		$(".edate").val(year + "-" + month + "-" + day);
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	
	function replacePlus(str){
		str = str.replace(/\+/gi," ");
		return str;
	};
	
	function replace(str){
		str = str.replace(/-/gi,".");
		return str;
	};
	

	
	function getTemperList(){
		var url = "${ctxPath}/sensor/getTemperList.do";
		var sdate = $('#sdate').val() + ' 00:00:00';
		var edate = $('#edate').val() + ' 23:59:59';
		var param = "sDate=" + sdate + "&eDate=" + edate;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "html",
			type : "post",
			success : function(data){
				$("#temperList").html('');
				$("#temperList").html(data);
				if ( $.fn.dataTable.isDataTable( '#temperList' ) ) {
					$("#temperList").dataTable({
						"destroy": true
					});
					$("#temperList").dataTable();
				}else{
					dataTable = $("#temperList").dataTable();
				};		
			}
		});
	};
	
	
	
	function csvData(ty, title){
		console.log('run csvData');
		var url = "";
		var param = "";
		if(ty=="temper"){
			url = "${ctxPath}/sensor/getTemperListCSV.do";
			var sdate = $('#sdate').val() + ' 00:00:00';
			var edate = $('#edate').val() + ' 23:59:59';
			param = "sDate=" + sdate + 
						"&eDate=" + edate;
		}
		
		$.ajax({
			url : url,
			type : "post",
			data : param,
			dataType : "json",
			success : function(json) {
				if(ty=="temper"){
					csvOutput = "NO, 센서명, 수집시간, 온도, 기록시간 LINE";
					var dscnt = json;
					var i = 1;
					$(dscnt).each(function(){
						var temperId = replacePlus(decodeURIComponent(this.temperId));
						var temperValue = replacePlus(decodeURIComponent(this.temperValue));
						var temperDatetime = replacePlus(decodeURIComponent(this.temperDatetime));
						var regdt = replacePlus(decodeURIComponent(this.regdt));
						
						
						csvOutput += i + "," + 
										temperId + "," +
										temperDatetime + "," +
										temperValue + "," +
										regdt + " LINE";
						i++;
					});
				}
				csvSend(title);
			}
		});
	};
	
	function csvSend(title){
		var sdate = title;
		var edate = "";
		if(title=="온도 센서 정보"){
			var date1 = $('#sdate').val();
			var date2 = $('#edate').val();
			edate = date1 + " - " + date2;			
		}

		csvOutput = csvOutput.replace(/\n/gi,"");
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sdate;
		f.endDate.value = edate;
		f.submit(); 
	};
	
	function setInit(ty){
		if(ty=="temper"){
			getTemperList();	
		}else if(ty=="dscnt"){
			getDscntList();
		}else if(ty=="cstmr"){
			getCstmrList();
		};
	};
</script>
<style type="text/css">
</style>
<title>Sensor Information</title>
</head>
<body>
	
	<div id="tabs" style="width: 100%">
		<ul>
		    <li><a href="#tab1" onclick="setInit('temper')">온도 센서 정보</a></li>
	  	</ul>
	  	
	   <div id="tab1">
			<center>
				<div style="font-weight: bold;">※ 날짜를 선택 하세요. ※</div>
				<form action="" id="temperListForm">
					<input type="date" id="sdate" class="sdate" data-role="none" > - 
					<input type="date" id="edate" class="edate" data-role="none" >
					<button onclick="getTemperList(); return false;" data-role="none"  class="csvBtn">조회</button>
					<button onclick="csvData('temper','온도 센서 정보'); return false;" data-role="none" id="csvBtn" class="csvBtn">엑셀 저장</button>
					<table border="1" width="100%" id="temperList" style="text-align: center;border-collapse: collapse; font-size: 9px">
					</table>
				</form>
			</center>
	   </div>
	</div>
	<!--csv  -->
	<form action='${ctxPath}/sensor/csv.do' id="f" method="post">
			<input type="hidden" name="csv" value="">
			<input type="hidden" name="startDate" value="">
			<input type="hidden" name="endDate" value="">
	</form>
</body> 
</html>