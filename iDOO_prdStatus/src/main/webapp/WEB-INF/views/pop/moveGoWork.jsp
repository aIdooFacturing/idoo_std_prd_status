<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/WEB-INF/views/include/unomiclib.jsp"%>
<%@ page import="com.unomic.dulink.common.domain.CommonCode"%>
<%@ page import="com.unomic.dulink.chart.domain.*"%>
<%@ page session = "true" %>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<style>
body{
	background: #484848;
	margin: 0;
	color: white;
	width: 100%;
	height: 100%;
	position: absolute;
}
#header{
	width: 100%;
	height: 15%;
}
#backBtn{
	background: linear-gradient( darkslateblue,#7112FF);
	border-radius: 5%;
	width: 20%;
	height: 100%;
	display: table;
	float: left;
	
}
#backBtn span{
	width:100%;
	height:100%;
	display: table-cell;
	vertical-align: middle;
	text-align: center;
	font-size: 300%;
}
#title{
	background: linear-gradient( #000000,#484848);
	width: 80%;
	height: 100%;
	display: table;
	float: left;
}
#title span{
	width:100%;
	height:100%;
	display: table-cell;
	vertical-align: middle;
	text-align: center;
	font-size: 500%;
}
#aside{
	background: green;
	width: 100%;
	height: 15%;
	display: table;
	background: darkorchid;
	color:indigo;
}
#aside span{
	width:100%;
	height:100%;
	display: table-cell;
	vertical-align: middle;
	text-align: center;
	font-size: 400%;
}
#content{
	width: 100%;
	height: 70%;
	background: #242424;
	overflow: auto;
}
</style>

<script>
	$(function(){
		//출근보고 히스토리 가져오기
		getTable();
		
		//focus TEXT 맞추기
		var chk_short = true;
		
		$(document).bind("keydown keyup", function(e) {
	        var key = e.keyCode;
	        var tg = e.target;
	        if(tg.tagName == "INPUT" ||  tg.tagName == "TEXTAREA") return true;
	        
	        var specific = key >= 8 && key <= 46;
	        if(e.type == "keydown") {
	            if(specific) {
	                chk_short = false;
	                return true;
	            }
	            if(!specific && chk_short) {
	            	 $("#empCd").focus().select();
	                //target_input.focus().select(); return false;
	            }
	            if(e.ctrlKey && e.keyCode == 86){
	            	 $("#empCd").focus().select();
	            }
	        } else {
	            if(specific) {
	                chk_short = true;
	            }
	        }
	    });
	})
	
	//enter key event
	function enterEvt(event) {
		if(event.keyCode == 13){
			saveRow()
		}
	}
	
	var evtMsg
	function alarmMsg(){
		clearTimeout(evtMsg)
		return evtMsg = setTimeout(function() {$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText'>바코드를 입력해주세요</marquee></span>")}, 10000)
	}
	//출근 DB 저장하기
	function saveRow(){
		$("#empCd").focus();
		$("#empCd").select();
		
		var empCd=$("#empCd").val();
		
		//앞에 4자리는 바코드 구분 현재 1111이면 사원증
		if(empCd.substring(0,4)!="1111"){
			$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:red;'>바코드를 다시 확인해 주세요</marquee></span>")
			alarmMsg();
			return;
		}
		
		empCd = empCd.substring(4,empCd.length);
		
		$.showLoading(); 
		
		var url = "${ctxPath}/pop/GoWorkSave.do";
		
		
		
		var param = "empCd=" + empCd+
 					"&sDate=" + moment().subtract(6,"month").format("YYYY-MM-DD") +
					"&eDate=" + moment().format("YYYY-MM-DD") ;  
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
//			dataType : "json",
			success : function(data){
				if(data=="no"){
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:red;'>등록된 사원번호가 아닙니다.</marquee></span>")
					alarmMsg();
				}else if(data=="duple"){
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:red;'>이미 출근처리 되었습니다.</marquee></span>")
					alarmMsg();
				}else if(data=="success"){
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:blue;'>정상적으로 출근처리 되었습니다.</marquee></span>")
					alarmMsg();
					getTable();
				}else if(data=="fail"){
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:blue;'>관리자에게 문의해주세요.</marquee></span>")
					alarmMsg();
				}
				
				$.hideLoading(); 
			},error : function(e){
				$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:blue;'>관리자에게 문의해주세요.</marquee></span>")
				alarmMsg();
				$.hideLoading(); 

			}
		});
		
	}
	
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	//출근 리스트 불러오기
	function getTable(){
		$.showLoading(); 
		classFlag = true;
		var tablelist=[];
		var url = "${ctxPath}/pop/getGoWorkList.do";
		
		
		
		var param = "deliveryNo=" + $("#deliveryNo").val()+
 					"&sDate=" + moment().subtract(6,"month").format("YYYY-MM-DD") +
					"&eDate=" + moment().format("YYYY-MM-DD") ;  
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				console.log("완성")
				console.log(data)
				var json=data.dataList;
				var table="<table style='width: 100%; text-align: center; font-size: 200%;color : darkgray;'>"
					table += "<tr><th colspan='3'>사원번호 : <input type='text' id='empCd' onkeyup='enterEvt(event)' style='padding: 0; margin: 0; vertical-align: middle;'></th></tr>"
					table += "<tr><th>이름</th><th>출근시간</th><th>퇴근시간</th></tr>";
				$(json).each(function(idx, data){
					data.nm = decode(data.nm);
					
					table += "<tr><td>" + data.nm + "</td>";
					table += "<td>" + data.startTime + "</td>";
					table += "<td>" + data.endTime + "</td></tr>";
				});
				
				$("#content").empty()
				$("#content").append(table)
				$.hideLoading(); 
			}
		});
	}
</script>

<body>
	<div id="header">
		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
			<span>BKJM</span>
		</div>
		<div id="title">
			<span>출 근 보 고</span>
		</div>
	</div>
	<div id="aside">
		<span>
			<marquee behavior=alternate scrollamount="20" id="alarmText" onabort="asd()" onended="dsa()" onsubmit="aa()" oncancel="das()" onbounce="bb()" onfinish="aa1()">
				바코드를 입력해주세요
			</marquee>
		</span>
	</div>
	<div id="content">
	</div>
</body>
</html>