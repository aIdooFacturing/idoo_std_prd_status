$('head').append("<link rel='stylesheet/less' type='text/css' href='" + ctxPath + "/template/unos_template.less'><\/link>");
$('head').append("<script src='" + ctxPath + "/template/less.min.js'><\/script>");
$('head').append("<script src='" + ctxPath + "/template/classie.js'><\/script>");
$('head').append("<script src='" + ctxPath + "/template/selectFx.js'><\/script>");

var targetWidth = 3840;
var targetHeight = 2160;

var originWidth = window.innerWidth;
var originHeight = window.innerHeight;

var contentWidth = originWidth;
var contentHeight = targetHeight/(targetWidth/originWidth);

var screen_ratio = getElSize(240);

if(originHeight/screen_ratio<9){
	contentWidth = targetWidth/(targetHeight/originHeight)
	contentHeight = originHeight; 
};

function getElSize(n){
	return contentWidth/(targetWidth/n);
};

var marginWidth = (originWidth-contentWidth)/2;
var marginHeight = (originHeight-contentHeight)/2;


$(function(){
	[].slice.call( document.querySelectorAll( 'select' ) ).forEach( function(el) {	
		new SelectFx(el);
	});
	
	var menu_box = document.createElement("div");
	menu_box.style.cssText = "width : " + getElSize(200) + ";" + 
							"height : " + getElSize(200) + ";" + 
							"position : absolute;" + 
							"top : 0px;" + 
							"left : 0px;";
	
	$("body").prepend(menu_box);
	
	//$(menu_box).load(ctxPath +"/template/menu_panel.html");
	
	setTime();
	setElement();
	
	$("#color").change(function(){
		var color = $(this).val();
		console.log(color);
		
		$(".table_title").css({
			"background-color" : color
		});
	})
	
});

var menu_flag = false;
function togglePanel(){
	if(!menu_flag){
		$("#panel").animate({
			"left" : 0
		}, 500);
		
		$("#menu").animate({
			"left" :$("#panel").width() + Number($("#panel").css("padding").substr(0, $("#panel").css("padding").indexOf("px")))*2
		}, 500);
	}else{
		$("#panel").animate({
			"left" : 0-($("#panel").width() + Number($("#panel").css("padding").substr(0, $("#panel").css("padding").indexOf("px")))*2)
		}, 500);
		
		$("#menu").animate({
			"left" : 0
		},500);
	}
	
	menu_flag = !menu_flag;
};

function setTime(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	var hour = addZero(String(date.getHours()));
	var minute = addZero(String(date.getMinutes()));
	var second = addZero(String(date.getSeconds()));
	var day_ = date.getDay();
	
	if(day_==1){
		day_ = "Mon";
	}else if(day_==2){
		day_ = "Tue";
	}else if(day_==3){
		day_ = "Wed";
	}else if(day_==4){
		day_ = "Thu";
	}else if(day_==5){
		day_ = "Fri";
	}else if(day_==6){
		day_ = "Sat";
	}else if(day_==0){
		day_ = "Sun";
	};
	
	var today = month + " / " + day + " (" + day_ + ")&nbsp&nbsp"+ hour + ":" + minute + ":" + second;
	
	$("time").html(today);
	setTimeout(setTime, 1000);
};

function addZero(str){
	if(str.length==1) str = "0" + str;
	return str;
};


function setElement(){
	$("h1").css({
		"margin-top" : getElSize(30),
		"font-size" : getElSize(80),
		"font-weight" : "bolder"
	});
	
	$("#container").css({
		"width" : contentWidth,
		"height" : contentHeight,
		"margin-top" : marginHeight,
		"margin-left" : marginWidth
	});
	
	$(".unos_table").css({
		"margin-left" : (contentWidth/2) - ($(".unos_table").width()/2),
	});
	
	$("time").css({
		"right" : $(".unos_table").offset().left
	});
	
	$("*").animate({
		"opacity" : 1
	})
};

